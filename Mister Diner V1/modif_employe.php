<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="modif_employe.css">
    <title>Modification employé</title>
</head>
<body>
<main class="card">
    <h1>Modifier un membre</h1>
<form action="Ajout_employe.php" method="post">
    <div>
        <label for="name">Nom :</label>
        <input type="text" id="name" name="employe_name">

    </div>
    <div>
        <label for="forname">Prénom : </label>
        <input type="text" id="forname" name="employe_forname">

    </div>
    <div>
        <label for="pseudo">Pseudo :</label>
        <input type="text" id="pseudo" name="employe_pseudo">
    </div>
    <div>
        <label for="score">Score :</label>
        <input type="number" id="score" name="employe_score">
    </div>
    <div id="validation">
        <input type="submit" value="Valider">
    </div>
</form>
</main>
</body>
</html>




<?php
?>