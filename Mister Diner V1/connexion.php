
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="connexion.css">
    <title>connexion</title>
</head>
<body>
<main id="main">
<form action="connexion.php" class="card"  method="post">
    <h1>Connexion</h1>
    <div class="mb-3">
        <label for="username" class="form-label">Pseudo :</label><br>
        <input type="text" class="form-control" id="username">
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Mot de passe :</label><br>
        <input type="password" class="form-control" id="password">
    </div>
    <div id="bouton_mdp">
    <a  href="#">Mot de passe oublié</a><br>
    </div>
    <div id="bouton_connexion" >
        <input type="submit" value="envoyer">
    </div>
</form>
</main>
</body>
</html>