<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Ajout d'une tâche</title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="Ajout_tache.css">
</head>
<body>
<main class="card">
<h1>Ajout d'une tâche</h1>
<form action="Ajout_tache.php" method="post">
    <div>
        <label for="name">Nom :</label>
        <input type="text" id="name" name="tache_name">

    </div>
    <div>
        <label for="description">Description :</label>
        <input type="text" id="description" name="tache_description">

    </div>
    <div>
        <label for="score">Score :</label>
        <input type="number" id="score" name="tache_score">

    </div>
    <div id="type">
        <label name="type" for="type">Type :</label>
        <select type="" id="type" name="tache_type">
            <option value="">Choissisez une option</option>
            <option value="courante">Récurrente</option>
            <option value="ponctuelle">Ponctuelle</option>
            <option value="surprise">Surprise</option>
        </select>
    </div>
    <div id="date">
        <label for="date">Date de fin :</label>
        <input type="date" id="date" name="date">
    </div>
    <div id="validation">
        <input type="submit" value="Valider">
    </div>
</form>
</main>
</body>
</html>