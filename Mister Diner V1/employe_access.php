<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="employe_access_style.css">
    <title>Accès employé</title>
</head>
<body>
<header>
    <h1>Accès employé</h1>
    <div id="deco">
        <a href="#">Déconnexion</a>
    </div>
    <div id="p score">
        <p>Score :</p>
    </div>
    <div id="tache_du_jour">
        <h3>Tâche du jour :</h3>
    </div>
</header>

<main>
    <h4>Tâches :</h4>
<div class="tableau">
    <form action="employe_access.php" METHOD="post">
        <table class="table_tache">
            <thead>
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Date</th>
                <th scope="col">Score</th>
            </tr>
            </thead>
            <tr>
                <td>Laver le frigo</td>
                <td>08/01/2021</td>
                <td>20</td>
                <td><input type="checkbox" id="check"></td>
            </tr>
            <tr>
                <td>Ranger le lave-vaisselle</td>
                <td>02/01/2021</td>
                <td>10</td>
                <td><input type="checkbox" id="check"></td>
            </tr>
        </table>
        <div id="boutons_taches">
            <input type="button" id="take" value="Je prends !">
            <input type="button" id="finish" value="J'ai fini !">
            <input type="button" id="weapon" " value="Je rends les armes..">
        </div>
    </form>
</div>
<div class="tableau" id="classement">
    <h4>Classement :</h4>
        <form action="employe_access.php" METHOD="post">
            <table class="table_classement">
                <thead>
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Score</th>
                </tr>
                </thead>
                <tr>
                    <td>Personne5</td>
                    <td>500 points</td>
                </tr>
                <tr>
                    <td>Personne2</td>
                    <td>420 points</td>
                </tr>
            </table>
        </div>
<div class="cadeau">
    <h4>Cadeaux :</h4>
    <img src="images/cadeau.jpeg" alt="liste des cadeaux">
</div>
</main>
</body>
</html>




<?php
?>