<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Enigma</title>
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
<header>
    <h1>Projet Enigma</h1>
    <a href="connexion.php">Se connecter</a>

</header>
<main>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active" id="slide1">
                <img class="d-block w-100" src="images/Donnas-Diner-Sunset-4.jpeg" alt="tache du jour">
                    <div class="carousel-caption">
                    <p id="paragraphe_tache">Tâche du jour :</p>
                    <a id="bouton_go" href="employe_access.php">J'y vais !</a>
                    </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="images/classement_general.png" alt="classement">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="images/cadeau.jpeg" alt="cadeaux">
            </div>
        </div>
        <div class="carousel-control">
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        </div>
    </div>
    <h2 id="classement">Classement :</h2>
<div class="progress">
    <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">pseudo1</div>
</div>
<br>
<div class="progress">
    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">pseudo2</div>
</div>
<br>
<div class="progress">
    <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">pseudo3</div>
</div>
<br>
<div class="progress">
    <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">pseudo4</div>
</div>
<br>
<div class="progress">
    <div class="progress-bar progress-bar-striped bg-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">pseudo5</div>
</div>


</main>
<footer>
    <p>Vers les autres pages</p>
    <ul>
        <li><a href="Attribution.php">Formulaire d'attribution</a></li>
        <li><a href="admin_access.php">Accès administrateur</a></li>
        <li><a href="Ajout_employe.php">Formulaire d'ajout employé</a></li>
        <li><a href="Ajout_tache.php">Formulaire d'ajout de tâches</a></li>
        <li><a href="modif_employe.php">Formulaire modification d'employé</a></li>
        <li><a href="Modification_tache.php">Formulaire de modification de tâches</a></li>
    </ul>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    $('.carousel').carousel({
        interval: 5000
    })
</script>
</body>
</html>
