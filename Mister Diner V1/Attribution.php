<html lang="fr">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="Attribution.css">
    <title>Attribution</title>
</head>
<body>
<main class="card">
<h1>Liste des tâches sélectionnées :</h1> <br>
    <p>tache1,tache2,tache3...</p>
<h3>Liste des employés</h3>
<form action="Attribution.php" METHOD="post">
<table class="table">
    <thead>
    <tr>
        <th scope="col">Prenom</th>
        <th scope="col">Nom</th>
        <th scope="col">Pseudo</th>
    </tr>
    </thead>
    <tr>
        <td>prenom1</td>
        <td>nom1</td>
        <td>@pseudo1</td>
        <td><input type="checkbox" id="check"></td>
    </tr>
    <tr>
        <td>prenom2</td>
        <td>nom2</td>
        <td>@pseudo2</td>
        <td><input type="checkbox"></td>
    </tr>
</table>
    <div id="validation">
<input type="submit" value="Valider">
    </div>
</form>
</main>
</body>
</html>

<?php
?>
