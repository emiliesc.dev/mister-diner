<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="admin_access.css">
    <title>Accès Administrateur</title>
</head>
<header>
    <h1>Accès administrateur</h1>
    <div id="deco">
        <input type="button" value="Déconnexion">
    </div>
</header>
<body>
<div class="tableau">
    <h4 id="p score">Score</h4>
    <form action="Attribution.php" METHOD="post">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Date</th>
                <th scope="col">Type</th>
                <th scope="col">Score</th>
            </tr>
            </thead>
            <tr>
                <td>Laver le frigo</td>
                <td>08/01/2021</td>
                <td>Ponctuelle</td>
                <td>20</td>
                <td><input type="checkbox" id="check"></td>
            </tr>
            <tr>
                <td>Ranger le lave-vaisselle</td>
                <td>02/01/2021</td>
                <td>Récurrente</td>
                <td>10</td>
                <td><input type="checkbox" id="check"></td>
            </tr>
        </table>
        <div class="boutons">
        <input type="button" class="ajout" value="Ajouter">
        <input type="button" class="modif" value="Modifier">
        <input type="button" class="suppr" value="Supprimer">
        <input type="button" class="attrib" value="Attribuer">
        </div>
    </form>
</div>

<div class="tableau">
    <h4>Membres</h4>
    <form action="Attribution.php" METHOD="post">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Prenom</th>
                <th scope="col">Pseudo</th>
                <th scope="col">Score</th>
            </tr>
            </thead>
            <tr>
                <td>Prenom1</td>
                <td>Nom1</td>
                <td>Pseudo1</td>
                <td>150</td>
                <td><input type="checkbox" id="check"></td>
            </tr>
            <tr>
                <td>Nom2</td>
                <td>Prenom2</td>
                <td>Pseudo2</td>
                <td>120</td>
                <td><input type="checkbox" id="check"></td>
            </tr>
        </table>
        <div class="boutons">
        <input type="button" class="ajout" value="Ajouter">
        <input type="button" class="modif" value="Modifier">
        <input type="button" class="suppr" value="Supprimer">
        </div>
    </form>
</div>
<div class="liste_anecdote">
    <form action="admin_access.php" method="post">
    <ol>
        <li>Personne1 à lavé le frigo 59 fois ! <input type="checkbox" id="check"></li>
        <li>Personne4 à nettoyé derrière les meubles ! <input type="checkbox" id="check"></li>
    </ol>
        <div id="bouton_anecdote">
    <input type="button" class="ajout" value="Ajouter">
    <input type="button" class="modif" value="Modifier">
    <input type="button" class="suppr" value="Supprimer">
        </div>
    </form>
</div>
<div class="cadeau">
    <h4>Cadeaux</h4>
    <form action="admin_access.php" method="post">
    <img src="images/cadeau.jpeg" alt="Liste des cadeaux disponibles"><br>
        <input type="file" accept=".image/*"><br>
        <input type="submit" value="Envoyer">
    </form>
</div>
</body>
</html>






<?php
?>