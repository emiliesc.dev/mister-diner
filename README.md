# Création du projet
Le but de cette application est de faire participer tout les employés aux tâches ménagères. Pour ce faire, on souhaite "gamifier" le concept en créant un système de points et de récompenses. Chaque personne va s'attribuer des tâches par jour, et à la validation de ces dernières, récupérer les points. 
Il y a plusieurs types de tâches : 
- Récurrentes
- Ponctuelles

Après un brainstorming, j'ai composé un backlog de mon projet et des schémas.

# Composition du projet
- Ecriture HTML pour toutes les pages de mon application en réutilisant les schémas fait précédemment. 

- Composition de mon diagramme de base de données

- Includes pour le menu,l'entête et le pied-de-page

- Un fichier images pour tout les visuels composant le projet

- Un fichier Uploads pour la partie cadeaux

- Un fichier fonts pour toutes les polices


# Etapes du projet

- Backlog (équivalent d'un cahier des charges)

- HTML et CSS de l'interface de connexion et de l'accès administrateur

- HTML et CSS de l'index

- Création de la base de données

- PHP de l'interface de connexion

- PHP de l'interface administrateur (tâches, puis employés et cadeaux)

- PHP de l'index





