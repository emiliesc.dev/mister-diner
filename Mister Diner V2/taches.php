<?php
$nom="Liste des tâches";
$bouton="ajouter une tâche";
$link="'../ajout_tache.php'";
require('includes/header.php');
require('includes/sidebar.php');
require('includes/bandeau.php');
require ('config.php');
$taches = $db->prepare("SELECT taches.score, taches.name, type, taches.id, `utilisateurs-id`, pseudo, DATE_FORMAT(date, '%d %M %Y') as date_tache FROM taches LEFT JOIN utilisateurs ON utilisateurs.id=taches.`utilisateurs-id` ORDER BY date ASC  ");
$taches->execute();
$taches = $taches->fetchAll(PDO::FETCH_ASSOC);
?>
    <div id="taches">
        <div class="tableau">
            <form action="accueil.php" METHOD="post">
                <label for="multiple_suppr"></label>
                <div class="table-responsive">
                    <table class="table align-middle" id="tableau_tache">
                        <?php
                        foreach( $taches as $tache){
                            if($tache['type']==1){
                                echo '<tr>';
                                echo '<td class="score"><strong>'.$tache['score'].'</strong>'.' ' .'<p>points</p>'.'</td>';
                                echo '<td class="date"><strong class="font">'.$tache['date_tache'].'</strong></td>';
                                echo '<td class="nom">'.$tache['name'].'</td>';
                                echo '<td class="statut"><img src="images/icon_reccurent.svg" alt="reccurente"></td></li>';
                                if($tache['utilisateurs-id']===null) {
                                    echo '<td  class="responsable" id="responsable-tableau"><img src="images/profil-picture.png" alt="avatar">Personne</td>';
                                }else{
                                    echo '<td class="responsable" id="responsable-tableau"><img src="images/profil-picture.png" alt="avatar">'.$tache['pseudo'].'</td>';
                                }
                                echo '<td class="profil-tache" id="img-tache1"><a href="modification_tache.php?id='.$tache['id'].'"><img src="images/icon_edit.svg" alt="formulaire modification"></a></td>';
                                echo '<td class="profil-tache" id="img-tache2"><a href="suppression_taches.php?id='.$tache['id'].'"><img src="images/icon_delete.svg" alt="formulaire suppression"></a></td>';
                                echo '<td class="checkbox-tableau align-middle"><input type="checkbox" id="checkbox1" name="supprim" value= '.$tache['id'].'"><label for="checkbox1"></label></td>';
                                echo '</tr>';

                            } elseif($tache['type']==2){
                                echo '<tr>';
                                echo '<td class="score"><strong>'.$tache['score'].'</strong>'.' ' .'<p>points</p>'.'</td>';
                                echo '<td class="date"><strong class="font">'.$tache['date_tache'].'</strong></td>';
                                echo '<td class="nom">'.$tache['name'].'</td>';
                                echo '<td class="statut" ><img src="images/icon_ponctuel.svg" alt="Ponctuelle"></td></li>';
                                if($tache['utilisateurs-id']==null) {
                                    echo '<td class="responsable" id="responsable-tableau"><img src="images/profil-picture.png" alt="avatar">Personne</td>';
                                }else{
                                    echo '<td class="responsable " id="responsable-tableau"><img src="images/profil-picture.png" alt="avatar">'.$tache['pseudo'].'</td>';
                                }
                                echo '<td class="profil-tache " id="img-tache1"><a href="modification_tache.php?id='.$tache['id'].'"><img src="images/icon_edit.svg" alt="formulaire modification"></a></td>';
                                echo '<td class="profil-tache" id="img-tache2"><a href="suppression_taches.php?id='.$tache['id'].'"><img src="images/icon_delete.svg" alt="formulaire suppression"></a></td>';
                                echo ' <td class="checkbox-tableau align-middle"><input type="checkbox" id="checkbox1"><label for="checkbox1"></label></td>';
                                echo '</tr>';

                            }}
                        ?>
                    </table>
            </form>
        </div>
    </div>


<?php
require("includes/footer.php");
?>