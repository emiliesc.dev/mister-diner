<?php
$nom="Liste des employés";
$bouton="Ajouter un employé";
$link="'../ajout_employe.php'";
require ('includes/header.php');
require ('includes/sidebar.php');
require ('includes/bandeau.php');
require ('config.php');
$employes = $db->prepare("SELECT name, forname, pseudo, score, id FROM utilisateurs");
$employes -> execute();
$employes = $employes->fetchAll(PDO::FETCH_ASSOC);
?>
    <div class="container1 container1-employe">
        <div class="container ajout_mobile" style="width:100%">
                    <?php
                    foreach( $employes as $employe) {
                        echo'<div class="col-sm">';
                        echo'  <div class="card card-employe" style="width:230px" >';
                        echo ' <img src="images/profil-picture.png" class="card-img-top" alt="Avatar">';
                        echo '<div class="card-header>';
                        echo '<h5 class="card-title" id="card_name">'. $employe['forname'].'</br>'.$employe['name'].'</h5>';
                        echo '<input type="checkbox" id="checkbox_employe">';
                        echo '</div>';
                        echo'    <div class="card-body">';
                        echo '<ul>';
                        echo '<li id="card_pseudo">' . $employe['pseudo'] . '</li>';
                        echo '<li id="card_score"><strong>' . $employe['score'] .'</strong><p>'. 'points'.'</p></li>';
                        echo '</ul>';
                        echo '</div>';
                        echo '<div class="card-footer">';
                        echo '<a href="modification_employe.php?id='.$employe['id'].'" class="card-link-modif"><img src="images/icon_edit.svg" alt="modifier"></a>';
                        echo '<a href="suppression_employe.php?id='.$employe['id'].'"class="card-link-suppr"><img src="images/icon_delete.svg" alt="supprimer"></a>';
                        echo '</div>';
                        echo'    </div>';
                        echo'  </div>';
                    }
                    ?>
                </div>
                </div>


<?php
require("includes/footer.php");
?>