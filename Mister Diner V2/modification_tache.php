<?php
$nom = 'Modifier une tâche';
require ('includes/sidebar.php');
require ('includes/bandeau.php');
require ('config.php');
$id=$_GET['id'];
if (!empty($_POST)) {
    $id = $_POST['tache_id'];
    $score = $_POST['tache_score'] ?? '';
    $description = $_POST['tache_description'] ?? '';
    $type = $_POST['tache_type'] ?? '';
    if (is_numeric($_POST['tache_resp'])) {
        $responsable = $_POST['tache_resp'];
    }else{
        $responsable = null;
    }
    $date = $_POST['date'] ?? '';
    if(isset($id)) {
        if (isset($_POST['tache_score'])
            or isset($_POST['tache_description'])
            or isset($_POST['tache_type'])
            or isset($_POST['tache_resp'])
            or isset($_POST['date'])) {
            $modif_tache = $db -> prepare("UPDATE taches SET score=:score , name=:description , type=:tache_type , `utilisateurs-id`=:responsable , date=:date WHERE id= :id" ) ;
            $modif_tache -> execute(array(':score' => $score,
                ':description' => $description,
                ':tache_type' => $type,
                ':responsable' => $responsable,
                ':date' => $date,
                ':id'=>$id));
        }
        else{
            echo'veuillez remplir tout les champs !';
        }}}
if(isset($id)) {
    $taches = $db->prepare("SELECT taches.score, date, taches.name,  `utilisateurs-id`, pseudo FROM taches LEFT JOIN utilisateurs ON utilisateurs.id=taches.`utilisateurs-id`  WHERE taches.id={$id} ");
    $taches -> execute();
    $taches = $taches -> fetch(PDO::FETCH_ASSOC);
}
?>
    <div class="container1">
        <div class="false_card">
            <h2 id="titre_modif">Modifier une tâche</h2>
            <form  id="modification_tache_form" method="post">
                <div id="points">
                    <label for="score">Points</label>
                    <input type="number" id="score" name="tache_score" value="<?php echo $taches['score'];  ?>">
                </div>
                <div id="description_tache">
                    <label for="description">Description</label>
                    <input type="text" id="description" name="tache_description" value="<?php echo $taches['name']; ?>">
                </div>
                <div id="type" class="input-group mb-3">
                    <div class="btn-group btn-group-toggle" data-toggle="buttons" id="type_tache">
                        <label for="tache_type">Type de tâche</label>
                        <label class="btn active">
                            <input type="radio" name="tache_type" value="1" checked><img src="images/icon_reccurent.svg" alt="reccurente"> Récurrente
                        </label>
                        <label class="btn">
                            <input type="radio" name="tache_type" value="2"><img src="images/icon_ponctuel.svg" alt="ponctuelle"> Ponctuelle
                        </label>
                    </div>
                    <div id="responsable_tache">
                        <label for="responsable">Responsable</label>
                        <select class="form-select" aria-label="Default select example" name="tache_resp" >
                            <option value="">Personne</option>
                            <?php
                            $liste_responsables = $db -> prepare('SELECT id, pseudo FROM utilisateurs');
                            $liste_responsables -> execute();
                            $liste_responsables = $liste_responsables -> fetchAll();
                            foreach ($liste_responsables as $liste_responsable) {
                                {
                                    ?>
                                    <option value="<?php echo $liste_responsable['id']; ?>"> <?php echo $liste_responsable['pseudo']; ?></option>
                                    <?php
                                }}
                            ?>
                        </select>
                    </div>
                    <div id="date">
                        <label for="date">Date d'échéance</label>
                        <input type="date" id="date" name="date" value="<?php echo $taches['date']; ?>">
                    </div>
                    <input type="hidden" name="tache_id" value="<?php echo $_GET['id'];?>">
                    <div id="validation_tache">
                        <input type="submit" value="Valider">
                    </div>
                    <div id="annulation_tache">
                        <input type="submit" value="Annuler">
                    </div>
            </form>
        </div>
    </div>
</div>



<?php
require ('includes/footer.php')
?>
