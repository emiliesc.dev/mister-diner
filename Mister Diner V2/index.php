<?php
require ('config.php');
$taches_jour = $db -> prepare('SELECT taches.name, taches.score, `utilisateurs-id` FROM taches WHERE `utilisateurs-id` IS NULL ORDER BY date ASC LIMIT 3');
$taches_jour -> execute();
$taches_jour = $taches_jour -> fetchAll(PDO::FETCH_ASSOC);
$taches_jour_ordonne = [];
$taches_jour_ordonne[0]=$taches_jour[1];
$taches_jour_ordonne[1]=$taches_jour[0];
$taches_jour_ordonne[2]=$taches_jour[2];

$employes = $db -> prepare ('SELECT name, forname, score FROM utilisateurs ORDER BY score DESC LIMIT 3 ');
$employes -> execute();
$employes = $employes -> fetchAll(PDO::FETCH_ASSOC);
$employe_ordonne = [];
$employe_ordonne[0] = $employes[1];
$employe_ordonne[1] = $employes[0];
$employe_ordonne[2] = $employes[2];
$classements = [1, 2, 3];
//$classements[0] = $classements[0];
//$classements[1]= $classements[1];
//$classements[2] = $classements[2];
$cadeau = $db -> prepare('SELECT url, date FROM upload ORDER BY date DESC LIMIT 1');
$cadeau -> execute();
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/index.css">
    <title>Accueil</title>
</head>
<body>
<div id="banniere">
    <img src="images/banniere.png" alt="bannière">
    <div id="bouton_page">
        <a href="accueil.php" id="bouton_page_connexion">Page de connexion</a>
    </div>
</div>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="images/fond.jpeg" class="d-block w-100" alt="taches du jour">
            <div class="carousel-caption d-none d-md-block">
                <div id="slide-tache">
                    <div id="barre1"> </div>
                    <h1>Taches du jour</h1>
                    <div id="barre2"> </div>
                    <div class="card-deck-tache">
                        <div class="card-deck">
                            <?php
                            foreach($taches_jour_ordonne as $id => $tache_jour) {
                                echo '<div class="nb-points tache-1" id="tache'.$id.'">';
                                echo '    <h4 class="points-tache"> <div class="nombre">' . $tache_jour['score'] . '</div> points</h4>';
                                echo '<div class="card-tache" id="premiere-tache">';
                                echo '<div class="card-body">';
                                echo '            <p class="card-text">' . $tache_jour['name'] . '</p>';
                                echo '</div>';
                                if($id===1) {
                                    echo '        <div class="card-footer">';
                                    echo '            <small class="text">Franchement stylé</small>';
                                    echo '        </div>';
                                }
                                echo '</div>';
                                echo '    </div>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="carousel-item" id="slide-cadeau">
        <img src="images/fond.jpeg" class="d-block w-100" alt="cadeaux">
        <div class="carousel-caption d-none d-md-block">
            <div id="barre1-cadeau"> </div>
            <h2>Cadeaux de l'année</h2>
            <div id="barre2-cadeau"> </div>
            <?php
            while($cadeaux = $cadeau -> fetch()) {
                echo'<table>';
                echo'<tr>';
                echo'<td>';
                echo '<img id="visuel-cadeau" src='.$cadeaux['url'].' alt="visuel cadeau">';
                echo'</td>';
                echo'</tr>';
                echo'</table>';
            }
            ?>
        </div>
    </div>
    <div class="carousel-item">
        <div id="slide-employe">
        <img src="images/fond.jpeg" class="d-block w-100" alt="classement">
        <div class="carousel-caption d-none d-md-block">
            <div id="barre1-classement"> </div>
            <h2 class="classement-title">Nos champions</h2>
            <div id="barre2-classement"> </div>
            <div class="nb-points-classement" id="classement-card">
                <div class="card-deck-employe">
            <div class="card-deck">
                    <?php
                    foreach ($employe_ordonne as $ids => $employe) {
                                echo '<div class="nb-points-employe" id="employe'.$ids.'">';
                        if($ids === 0){
                            echo '<h3>'.$classements[1].'</h3>';
                        }elseif ($ids === 1) {
                            echo '<h3>'.$classements[0].'</h3>';
                        }elseif ($ids === 2) {
                            echo '<h3>'.$classements[2].'</h3>';
                        }
                        echo '<img id="img-classement" class="card-img-top" src="images/profil-picture.png" alt="avatar">';
                        echo '<div class="card-header-classement">';
                        echo '<div class="card-text-classement">'.$employe['forname'].'<br>'.$employe['name'] . '</p></div>';
                        echo '</div>';
                        echo '<div class="card-body-classement">';
                        echo '<h4 id="points-classement"> <div class="nombre-points">' . $employe['score'] . '</div> points</h4>';
                        echo '</div>';
                        if($ids === 1) {
                            echo '<div class="card-footer">';
                            echo '<small class="text">Coeur sur toi</small>';
                            echo '</div>';
                            echo '</div>';
                        }elseif ($ids === 0) {
                            echo '<div class="card-footer">';
                            echo '<small class="text">Presque</small>';
                            echo '</div>';
                            echo '</div>';
                        }elseif ($ids === 2) {
                            echo '<div class="card-footer">';
                            echo '<small class="text">C est moche</small>';
                            echo '</div>';
                            echo '</div>';
                        }
                            }
                            ?>
            </div>
            </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<script>
    $('.carousel').carousel({
        interval: 5000
    });
</script>
</body>
</html>

