<?php
$name="Tâches disponibles";
require('includes/header_employe.php');
require('includes/sidebar_employe.php');
require('includes/bandeau_employe.php');
require('config.php');
$taches = $db->prepare("SELECT taches.score, taches.name, type, taches.id, `utilisateurs-id`, pseudo, DATE_FORMAT(date, '%d %M %Y') as date_tache FROM taches LEFT JOIN utilisateurs ON utilisateurs.id=taches.`utilisateurs-id` ORDER BY date ASC  ");
$taches->execute();
$taches = $taches->fetchAll(PDO::FETCH_ASSOC);
?>
<div id="taches">
    <div class="tableau">
        <form action="accueil.php" METHOD="post">
            <label for="multiple_suppr"></label>
            <div class="table-responsive">
                <table class="table align-middle" id="tableau_tache">
                    <?php
                    foreach( $taches as $tache){
                        if($tache['utilisateurs-id']== null){
                        if($tache['type']==1){
                            echo '<tr>';
                            echo '<td class="score"><strong>'.$tache['score'].'</strong>'.' ' .'<p>points</p>'.'</td>';
                            echo '<td class="date"><strong class="font">'.$tache['date_tache'].'</strong></td>';
                            echo '<td class="nom">'.$tache['name'].'</td>';
                            echo '<td class="statut"><img src="images/icon_reccurent.svg" alt="reccurente"></td></li>';
                            echo '<td class="profil-tache" id="img-tache2"><a href="mes_taches_version_employe.php?id='.$tache['id'].'">Je la prends !</a></td>';
                            echo '</tr>';

                        } elseif($tache['type']==2){
                            echo '<tr>';
                            echo '<td class="score"><strong>'.$tache['score'].'</strong>'.' ' .'<p>points</p>'.'</td>';
                            echo '<td class="date"><strong class="font">'.$tache['date_tache'].'</strong></td>';
                            echo '<td class="nom">'.$tache['name'].'</td>';
                            echo '<td class="statut" ><img src="images/icon_ponctuel.svg" alt="Ponctuelle"></td></li>';
                            echo '<td class="profil-tache" id="img-tache2"><a href="mes_taches_version_employe.php?id='.$tache['id'].'">Je la prends !</a></td>';
                            echo '</tr>';

                        }}}
                    ?>
                </table>
        </form>
    </div>
</div>
</div>
    <?php
    require("includes/footer.php")
    ?>

