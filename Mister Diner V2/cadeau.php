<?php
$nom = ' Visuel cadeaux';
require ('includes/header.php');
require ('includes/sidebar.php');
require ('includes/bandeau.php');
require ('config.php');
if (isset($_FILES['bouton_visuel'])) {
    $extensions = array('.png', '.jpg', '.jpeg');
    $extension = strrchr($_FILES['bouton_visuel']['name'], '.');
    if ($_FILES['bouton_visuel']['type'] == 'image/jpeg') { $extension = '.jpeg'; }
    if ($_FILES['bouton_visuel']['type'] == 'image/jpg') { $extension = '.jpg'; }
    if ($_FILES['bouton_visuel']['type'] == 'image/png') { $extension = '.png'; }
    if ($_FILES['bouton_visuel']['size'] < 3000000) {
        $name=rand(10000, 99999).$extension;
        move_uploaded_file($_FILES['bouton_visuel']['tmp_name'], 'uploads/'.$name);
    } else {
        echo 'Fichier trop gros';

    }
    $url = 'uploads/'.$name;
}
            if (isset($url)) {
                $image_bd = $db->prepare('INSERT INTO upload (url) 
                              VALUES (:url)');
                $image_bd->execute(array(
                    ':url'=>$url,
               ));
            }

            $affichage_image=$db -> prepare('SELECT url FROM upload ORDER BY date DESC LIMIT 1');
            $affichage_image -> execute();
?>
    <div class="container1">
        <div id="cadeau" class="tab-pane">
            <form enctype="multipart/form-data" method="post">
                <label for="bouton_visuel" class="label_visuel">Choisir une image</label>
                <input type="file" accept="image/jpeg,image/png"  id="bouton_visuel" required name="bouton_visuel">
                  <label for="bouton_image_submit" class="envoi_image">Valider</label>
                  <input type="submit"  id="bouton_image_submit"  name="bouton_visuel">
            </form>
            <div id="visuel_cadeau" >
                <?php
                while($affichage_images = $affichage_image -> fetch()) {
                    echo'<table>';
                    echo'<tr>';
                    echo'<td>';
                    echo '<img src='.$affichage_images['url'].'>';
                    echo'</td>';
                    echo'</tr>';
                    echo'</table>';
                }
                ?>
            </div>
        </div>
    </div>

<?php
require('includes/footer.php')
?>
