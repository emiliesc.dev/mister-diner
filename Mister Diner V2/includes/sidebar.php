<?php
require('header.php');
?>
<div class="sidebar">
    <a href="../accueil.php"><img id="logo" src="../images/logo.png" alt="Logo mister diner"></a>
    <ul class="nav nav-pills">
        <li class="nav-item">
            <a class="nav-link active" aria-current="page"  href="../taches.php">Tâches <img src="../images/play-solid.svg" alt="fleche"></a>

        </li>
        <li class="nav-item">
            <a class="nav-link"  href="../cadeau.php">Cadeaux <img src="../images/play-solid.svg" alt="fleche"></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="../employe.php">Employés <img src="../images/play-solid.svg" alt="fleche"></a>
        </li>
    </ul>
</div>

