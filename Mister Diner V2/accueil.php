<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/accueil.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <title>Espace connexion</title>
</head>
<body>
<main>
    <div id="diner-voiture">
        <img class="d-block w-100" src="images/bg_diner.jpg" alt="tache du jour">
    </div>

    <a href="index.php"><img id="logo" src="images/logo.png" alt="mister diner, logo">
    <div id="formulaire_co">
        <form action="accueil.php" method="post">
            <h1>Connexion</h1>
            <div class="mb-3">
                <label for="pseudo" class="form-label">Pseudo :</label><br>
                <input type="text" class="form-control" name="pseudo" id="pseudo">
            </div>
            <div class="mb-3">
                <label for="mdp" class="form-label">Mot de passe :</label><br>
                <input type="password" name="mdp" class="form-control" id="mdp">
            </div>
            <div id="bouton_connexion" >
                <input type="submit" value="Connexion">
            </div>
            <div id="bouton_mdp">
                <a  href="#">Mot de passe oublié</a><br>
            </div>

            <?php
            require ('config.php');
            $pseudo = $_POST['pseudo'] ?? '';
            $pass = $_POST['mdp'] ?? '';

            if (isset($_POST['pseudo']) && isset($_POST['mdp'])) {
                $req = $db -> prepare('SELECT * FROM utilisateurs WHERE pseudo = :pseudo AND mot_de_passe = :pass');
                $req -> execute(array(
                        ':pseudo' => $_POST['pseudo'],
                        ':pass' =>$_POST['mdp']));
                $resultat = $req -> fetch();
                if (!$resultat) {
                    echo '<div class="error-login">Vos identifiants sont incorrects !</div>';
                }
                else {
                    header('Location:../taches.php');
                    session_start();
                    $_SESSION['pseudo'] = $resultat['pseudo'];
                    $_SESSION['mdp'] = $pass;
                }
            }
            ?>
        </form>
    </div>
</main>
</body>
</html>
