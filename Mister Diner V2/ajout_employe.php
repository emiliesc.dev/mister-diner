<?php
$nom="Ajout d'un employé";
require('includes/sidebar.php');
require('includes/bandeau.php');
require ('config.php');
function motDePasse($longueur) {
    $Chaine = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $Chaine = str_shuffle($Chaine);
    $Chaine = substr($Chaine,0,$longueur);
    return $Chaine;
}
$nom = $_POST['employe_nom'] ?? '';
$prenom = $_POST['employe_prenom'] ?? '';
$pseudo = $_POST['employe_pseudo'] ?? '';
$score = $_POST['employe_score'] ?? '';
$password = motDePasse(10);

if (!empty($_POST['employe_nom'])
    && !empty($_POST['employe_prenom'])
    && !empty($_POST['employe_pseudo'])
    && !empty($_POST['employe_score'])) {
    $ajout_tache = $db->prepare('INSERT INTO utilisateurs (name,forname,pseudo,mot_de_passe,score ) 
                                                        VALUES (:name,:forname,:pseudo,:mdp,:score)');
    $ajout_tache->execute(array(':name' => $nom,
        ':forname' => $prenom,
        ':pseudo' => $pseudo,
        ':mdp'=>$password,
        ':score' => $score,
    ));
} else {
    echo"Veuillez remplir tout les champs !";
}

?>
    <div class="container1">
    <div class="false_card">
        <h2>Ajouter un employé</h2>
        <form action="ajout_employe.php" method="post">
            <div id="nom">
                <label for="nom">Nom</label>
                <input type="text" id="nom" name="employe_nom">
            </div>
            <div id="prenom">
                <label for="prenom">Prénom</label>
                <input type="text" id="prenom" name="employe_prenom">
            </div>
            <div id="pseudo">
                <label for="pseudo">Pseudo</label>
                <input type="text" id="pseudo" name="employe_pseudo">
            </div>
            <div id="points_employe">
                <label for="score_employe">Points</label>
                <input type="number" id="score_employe" name="employe_score">
            </div>
            <div id="groupe_boutons">
                <div id="validation_employe">
                    <input type="submit" value="Valider">
                </div>
                <div id="annulation_employe">
                    <input type="submit" value="Annuler">
                </div>
            </div>
        </form>
    </div>
    </div>

<?php
require ('includes/footer.php')
?>