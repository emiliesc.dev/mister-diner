<?php
$nom = "Modification d'un profil";
require ('includes/sidebar.php');
require ('includes/bandeau.php');
require ('config.php');

$id = $_GET['id'];
if (!empty($_POST)) {
    $nom = $_POST['employe_nom'] ?? "";
    $prenom = $_POST['employe_prenom'] ?? "";
    $pseudo = $_POST['employe_pseudo'] ?? "";
    $score = $_POST['employe_score'] ?? "";
    if (isset($id)) {
        $ajout_tache = $db -> prepare("UPDATE utilisateurs SET name=:name , forname=:forname , pseudo=:pseudo , score= :points WHERE id=:id" ) ;
        $ajout_tache -> execute(array(':name' => $nom,
            ':forname' => $prenom,
            ':pseudo' => $pseudo,
            ':points' => $score,
            ':id'=>$id));
    } else{
        echo 'veuillez remplir tout les champs !';
    }}

$id = $_GET['id'];
if (isset($id)) {
    $employe_form = $db -> prepare("SELECT name,forname,score, pseudo FROM utilisateurs  WHERE id={$id} ");
    $employe_form -> execute();
    $employe_form = $employe_form -> fetch(PDO::FETCH_ASSOC);
}
?>
        <div class="container1">
            <div class="false_card">
                <h2>Modifier un employé</h2>
                <form  method="post">
                    <div id="nom">
                        <label for="nom">Nom</label>
                        <input type="text" id="nom" name="employe_nom" value="<?php echo $employe_form['name'];?>">
                    </div>
                    <div id="prenom">
                        <label for="prenom">Prénom</label>
                        <input type="text" id="prenom" name="employe_prenom" value="<?php echo $employe_form['forname'];  ?>">
                    </div>
                    <div id="pseudo">
                        <label for="pseudo">Pseudo</label>
                        <input type="text" id="pseudo" name="employe_pseudo" value="<?php echo $employe_form['pseudo'];  ?>">
                    </div>
                    <div id="points_employe">
                        <label for="score_employe">Points</label>
                        <input type="number" id="score_employe" name="employe_score" value="<?php echo $employe_form['score'];  ?>">
                    </div>
                    <div id="groupe_boutons">
                        <div id="validation_employe">
                            <input type="submit" value="Valider">
                        </div>
                        <div id="annulation_employe">
                            <input type="submit" value="Annuler">
                        </div>
                    </div>
                </form>
            </div>
        </div>

<?php
require ('includes/footer.php')
?>